//
//  ViewController.swift
//  VikiCurrencyAssignment
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var baseCurrency: UILabel!
    @IBOutlet weak var targetCurrency: UILabel!
    @IBOutlet weak var inputMoney: UITextField!
    @IBOutlet weak var output: UILabel!
    var errorMessage : String = ""
    let exchangeRatesManager = ExchangeRateManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        baseCurrency.text =  exchangeRatesManager.currentRate.pair.inputCurrency
        targetCurrency.text = exchangeRatesManager.currentRate.pair.outputCurrency
        inputMoney.text = "1"
        inputMoney.returnKeyType = .done
        inputMoney.delegate = self
        output.text = "\(exchangeRatesManager.currentRate.rate)"
        exchangeRatesManager.didUpdateCurrentRate = { c in
            DispatchQueue.main.async {
                self.baseCurrency.text = c.pair.inputCurrency
                self.targetCurrency.text = c.pair.outputCurrency
                self.updateOutput()
            }
        }
        exchangeRatesManager.reloadExchangeRates()
        
    }
    
    func updateOutput() {
        if let input = Double(inputMoney.text ?? "1") {
            let pair = ExchangePair(inputCurrency: baseCurrency.text!, outputCurrency: targetCurrency.text!)
            if let rate = exchangeRatesManager.exchangeRates[pair] {
                output.text = "\(Double(input * rate))"
            }
            else {
                exchangeRatesManager.reloadExchangeRates(baseCurrency.text!)
            }
        }
    }
    
    @objc func goToCurrencyPicker() {
        let vc = Storyboards.Main.instantiateCurrencyPicker()
        vc.baseCurrencies = Array(exchangeRatesManager.availableCurrencies)
        vc.targetCurrencies = Array(exchangeRatesManager.availableCurrencies)
        vc.firstPair = ExchangePair(inputCurrency: baseCurrency.text!, outputCurrency: targetCurrency.text!)
        vc.didSelectCurrency = { pair in
            if let val = self.exchangeRatesManager.exchangeRates[pair] {
                self.exchangeRatesManager.currentRate = RateExchangePair(pair: pair, rate: val)
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func changeCurrency(_ sender: Any) {
        self.goToCurrencyPicker()
    }
    
}

extension ViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateOutput()
        textField.resignFirstResponder()
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateOutput()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: textField, action: #selector(resignFirstResponder))
        self.navigationItem.setRightBarButtonItems([doneButton], animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
