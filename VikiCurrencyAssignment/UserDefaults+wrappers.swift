//
//  UserDefaults+wrappers.swift
//  VikiCurrencyAssignment
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import Foundation

@propertyWrapper
struct UserDefaultCodablePW<T: Codable> {
    let key: String
    let defaultValue: T
    var defaultsSuite: UserDefaults
    var errorHandler: ((Error)->Void)?
    
    init(_ key: String, defaultValue: T, defaultsSuite: UserDefaults = .standard, errorHandler: ((Error)->Void)? = nil) {
        self.key = key
        self.defaultValue = defaultValue
        self.defaultsSuite = defaultsSuite
        self.errorHandler = errorHandler
    }

    var wrappedValue: T {
        get {
            guard let encoded = UserDefaults.standard.object(forKey: key) as? Data else {
                return defaultValue
            }
            do {
                let val = try JSONDecoder().decode(T.self, from: encoded)
                return val
            }
            catch {
                errorHandler?(error)
                return defaultValue
            }
        }
        set {
            do {
                let encoder = JSONEncoder()
                let encoded = try encoder.encode(newValue)
                defaultsSuite.set(encoded, forKey: key)
            }
            catch {
                errorHandler?(error)
            }
        }
    }
}
