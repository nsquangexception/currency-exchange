//
//  ExchangeRatesRequest.swift
//  VikiCurrencyAssignment
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import Foundation

struct ExchangeRate : Codable {
    var base : String
    var date : Date
    var rates : [String : Double]
}

struct ExchangeRatesRequest : APIRequest {
    typealias RequestDataType = String
    typealias ResponseDataType = ExchangeRate
    
    struct APIError: Codable, Error {
        var error : String
    }
    
    func makeRequest(from data: String) throws -> URLRequest {
        if let u = URLComponents(host: "api.exchangeratesapi.io", path: ["latest"], queries: [URLQueryItem(name: "base", value: data)]).url {
            var request = URLRequest.init(url: u)
            request.httpMethod = "GET"
            debugPrint(request)
            return request
        }
        else {
            throw APIRequestError.invalidURL
        }
    }
    
    func parseResponse(data: Data) throws -> ExchangeRate {
        debugPrint(String(data: data, encoding: .utf8) ?? "encoding data failed")
        let decoder = JSONDecoder()
        let f = DateFormatter()
        f.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(f)
        do {
            let r = try decoder.decode(ExchangeRate.self, from: data)
            return r
        }
        catch {
            if let apiError = try? decoder.decode(ExchangeRatesRequest.APIError.self, from: data) {
                throw apiError
            }
            throw error
        }
    }
    
}
