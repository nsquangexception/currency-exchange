//
//  CurrencyPickerViewController.swift
//  VikiCurrencyAssignment
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import UIKit

class CurrencyPickerViewController: UIViewController {
    @IBOutlet weak var picker: UIPickerView!
    
    var didSelectCurrency : ((ExchangePair)->Void)?
    var baseCurrencies = [String]()
    var targetCurrencies = [String]()
    var firstPair : ExchangePair?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        picker.dataSource = self
        picker.delegate = self
        if let f = firstPair, let baseIndex = baseCurrencies.firstIndex(of: f.inputCurrency), let targetCIndex = targetCurrencies.firstIndex(of: f.outputCurrency) {
            picker.selectRow(baseIndex, inComponent: 0, animated: true)
            picker.selectRow(targetCIndex, inComponent: 1, animated: true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillDisappear(_ animated: Bool) {
        let inputCurrency = baseCurrencies[picker.selectedRow(inComponent: 0)]
        let outputCurrency = targetCurrencies[picker.selectedRow(inComponent: 1)]
        self.didSelectCurrency?(ExchangePair(inputCurrency: inputCurrency, outputCurrency: outputCurrency))
    }
}
extension CurrencyPickerViewController : UIPickerViewDelegate {
    
}
extension CurrencyPickerViewController : UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return baseCurrencies.count
        case 1:
            return targetCurrencies.count
        default:
            return 0
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return baseCurrencies[row]
        case 1:
            return targetCurrencies[row]
        default:
            return nil
        }
    }
}
