//
//  APIRequest.swift
//  VikiCurrencyAssignment
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import Foundation

protocol APIRequest {
    associatedtype RequestDataType
    associatedtype ResponseDataType
    
    func makeRequest(from data: RequestDataType) throws -> URLRequest
    func parseResponse(data: Data) throws -> ResponseDataType
}

class APIRequestLoader<T: APIRequest> {
    let apiRequest: T
    let urlSession: URLSession
    
    init(apiRequest: T, urlSession: URLSession = .shared) {
        self.apiRequest = apiRequest
        self.urlSession = urlSession
    }
    func loadAPIRequest(requestData: T.RequestDataType,
                        completionHandler: @escaping (Result<T.ResponseDataType, Error>) -> Void) {
        do {
            let urlRequest = try apiRequest.makeRequest(from: requestData)
            urlSession.dataTask(with: urlRequest) { data, response, error in
                guard let data = data else {
                    if let e = error {
                        return completionHandler(.failure(e))
                    }
                    else {
                        return completionHandler(.failure(APIRequestError.missingData))
                    }
                }
                do {
                    let parsedResponse = try self.apiRequest.parseResponse(data: data)
                    completionHandler(.success(parsedResponse))
                } catch (let parsedError) {
                    completionHandler(.failure(parsedError))
                }
            }.resume()
        } catch (let requestError) {
            return completionHandler(.failure(requestError))
        }
        
    }
}

enum APIRequestError : Error {
    case invalidURL
    case missingData
}

extension URLComponents {
    init(scheme: String = "https", host: String, path: [String], queries: [URLQueryItem]?) {
        self = URLComponents.init()
        self.scheme = scheme
        self.host = host
        var p = path
        p.insert("/", at: 0)
        self.path = NSString.path(withComponents: p)
        self.queryItems = queries
    }
}
