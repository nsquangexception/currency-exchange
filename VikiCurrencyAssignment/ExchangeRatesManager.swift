//
//  ExchangeRatesManager.swift
//  VikiCurrencyAssignment
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import Foundation

struct ExchangePair : Hashable, Codable {
    var inputCurrency : String
    var outputCurrency : String
}

struct RateExchangePair : Codable {
    let pair : ExchangePair
    let rate : Double
}

class ExchangeRateManager  {
    static let shared = ExchangeRateManager()
    
    static let availableCurrenciesOfiOS = Locale.isoCurrencyCodes
    // @UserDefaultPW("exchangeRates", defaultValue: [ExchangePair : Double ]())
    //private(set) var exchangeRates : [ExchangePair : Double ]
    private(set) var exchangeRates = [ExchangePair : Double ]()
    
    @UserDefaultCodablePW("CurrentRate", defaultValue: RateExchangePair(pair: ExchangePair(inputCurrency: "USD", outputCurrency: "SGD"), rate: 1.3582009157) )
    var currentRate : RateExchangePair {
        didSet {
            self.didUpdateCurrentRate?(self.currentRate)
        }
    }
    
    var didUpdateCurrentRate: ((RateExchangePair) -> Void)?
    var availableCurrencies : Set<String> =  ["USD","SGD"]
    
    init() {
        
        ExchangeRateManager.availableCurrenciesOfiOS.forEach {
            self.reloadExchangeRates($0)
        }
    }
    
    func reloadExchangeRates(_ base : String = Locale.current.currencyCode ?? "USD") {
        let loader = APIRequestLoader<ExchangeRatesRequest>.init(apiRequest: ExchangeRatesRequest.init())
        loader.loadAPIRequest(requestData: base, completionHandler: {result in
            switch result {
            case .success(let rates):
                debugPrint(rates)
                rates.rates.forEach { (r, val) in
                    let pair = ExchangePair(inputCurrency: rates.base, outputCurrency: r)
                    self.exchangeRates[pair] = val
                    let reversePair = ExchangePair(inputCurrency: r, outputCurrency: rates.base)
                    self.exchangeRates[reversePair] = Double(1)/val
                    self.availableCurrencies.insert(r)
                    self.availableCurrencies.insert(rates.base)
                }
            case .failure(let err):
                debugPrint(err)
            }
        })
    }
}
