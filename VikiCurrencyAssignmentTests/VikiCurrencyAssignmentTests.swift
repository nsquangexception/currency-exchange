//
//  VikiCurrencyAssignmentTests.swift
//  VikiCurrencyAssignmentTests
//
//  Created by Quang on 11/3/19.
//  Copyright © 2019 Quang. All rights reserved.
//

import XCTest
@testable import VikiCurrencyAssignment

class VikiCurrencyAssignmentTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func _testRunAPIRequest() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let loader = APIRequestLoader<ExchangeRatesRequest>.init(apiRequest: ExchangeRatesRequest.init())
        let currencyCode =
            //["USD", "JPY", "EUR"].randomElement()!
            Locale.isoCurrencyCodes.randomElement()!
        let requestComplete = XCTestExpectation(description: "request complete")
        loader.loadAPIRequest(requestData: currencyCode, completionHandler: {result in
            switch result {
            case .success(let rates):
                print(rates)
            case .failure(let err):
                XCTFail("\(err)")
            }
            requestComplete.fulfill()
        })
        wait(for: [requestComplete], timeout: 3)
    }
    
    func testParseResponse() throws {
        let responseJSON = """
{
  "rates": {
    "CAD": 0.9090458795,
    "HKD": 5.4051142344,
    "ISK": 85.5055414525,
    "PHP": 34.8498544982,
    "DKK": 4.625843601,
    "HUF": 203.2877221225,
    "CZK": 15.7971642623,
    "GBP": 0.5325243019,
    "RON": 2.9439044022,
    "SEK": 6.6245433719,
    "IDR": 9684.1867376633,
    "INR": 48.7994551421,
    "BRL": 2.7513466658,
    "RUB": 44.0087920253,
    "HRK": 4.6189090459,
    "JPY": 74.5650424122,
    "THB": 20.817906012,
    "CHF": 0.6818772831,
    "EUR": 0.6191567086,
    "MYR": 2.8728871277,
    "BGN": 1.2109466906,
    "TRY": 3.9478050895,
    "CNY": 4.856665222,
    "NOK": 6.2929849545,
    "NZD": 1.0727509133,
    "ZAR": 10.4191690917,
    "USD": 0.6896786577,
    "MXN": 13.1981920624,
    "SGD": 0.9367221844,
    "AUD": 1,
    "ILS": 2.4315522259,
    "KRW": 804.9594452356,
    "PLN": 2.6335830599
  },
  "base": "AUD",
  "date": "2019-11-01"
}
"""
        let _ = try ExchangeRatesRequest().parseResponse(data: responseJSON.data(using: .utf8)!)
    }
}
