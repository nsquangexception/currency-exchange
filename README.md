#  Currency converter app

API for currency exchange: https://exchangeratesapi.io 

Screens: currency exchange, currency picker

Architecture: MVC

- Patterns for networking from [WWDC2018](https://devstreaming-cdn.apple.com/videos/wwdc/2018/417j8ucs9p8w7seip/417/417_testing_tips__tricks.pdf)

Tools to generate code: 

- Natalie: for type safe code with Segue and Identifiers

Persistence:

- Latest Exchange Currencies